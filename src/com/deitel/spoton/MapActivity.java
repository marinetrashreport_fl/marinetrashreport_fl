package com.deitel.spoton;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.maps.*;


public class MapActivity extends FragmentActivity {
	/**Declares the GoogleMap variable map*/
	private GoogleMap map;
	 
	 private LatLngBounds Florida = new LatLngBounds(
	 new LatLng(24, -80), new LatLng(24, -82));
	// Set the camera to the greatest possible zoom level that includes the
	// bounds
	//map.moveCamera(CameraUpdateFactory.newLatLngBounds(AUSTRALIA, 0));
	 /** Creates variables with the LatLng*/
	//private static final LatLng FAU_EE = new LatLng(26.365668,-80.106093);
	private static final LatLng Delray_Beach = new LatLng(26.46201,-80.05790);
	private static final LatLng lake_Worth = new LatLng(26.61676, -80.06845);
	private static final LatLng boyton_beach = new LatLng(26.53179, -80.09055);
	private static final LatLng jupiter_beach = new LatLng(26.93422, -80.09421);
	private static final LatLng juno_beach= new LatLng(26.87978, -80.05337);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        /** Creates the Google Map*/
        map = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        if (map == null) {
            Toast.makeText(this, "Google Maps not available", 
                Toast.LENGTH_LONG).show();
        }
        /** Calls the marker function to add the markers to the map*/
        marker();
        /** Sets the location to LatLng(26.93422, -80.09421) and sets the zoom 9.5f*/
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(26.93422, -80.09421), 9.5f));
    }
    
    
    /** Action bar is implemented*/
 	@Override
 	public boolean onCreateOptionsMenu(Menu menu) {
 		// Inflate the menu; this adds items to the action bar if it is present.
 		MenuInflater inflater = getMenuInflater();
 		getActionBar().setDisplayHomeAsUpEnabled(true);
 		inflater.inflate(R.menu.main_action_bar, menu);
 	    return super.onCreateOptionsMenu(menu);
 	}
 	
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 	    // Handle presses on the action bar items
 		/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
 	    switch (item.getItemId()) {
 	        case R.id.start_game:
 	            Start_Game();
 	            return true;
 	        case R.id.harmful_effects:
 	            Harmful_Effects();
 	            return true;
 	        case R.id.w_help:
 	        	w_help();
 	            return true;
 	        case R.id.help:
 	        	help();
 	            return true;
 	        case R.id.report:
	        	report();
	            return true;
 	        case R.id.menu_addmarker:
 	        	marker();
 	        	return true;

 	        default:
 	            return super.onOptionsItemSelected(item);
 	    }
 	}

 	private void marker() {
		/** Add markers to the map*/
    	 
        
         
         map.addMarker(new MarkerOptions() 
             .position(lake_Worth )
             .title("Lake Worth Beach") .snippet("Lake Worth Beach Debris Site")
             .icon(BitmapDescriptorFactory
             .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
         
         map.addMarker(new MarkerOptions() 
         .position(Delray_Beach)
         .title("Delray Beach") .snippet("Delray Beach Debris Site")
         .icon(BitmapDescriptorFactory
         .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
         
         map.addMarker(new MarkerOptions() 
         .position(boyton_beach )
         .title("Boyton Beach ") .snippet("Boyton Beach Debris Site")
         .icon(BitmapDescriptorFactory
         .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
         
         map.addMarker(new MarkerOptions() 
         .position(jupiter_beach )
         .title("Jupiter Beach") .snippet("Jupiter Beach Debris Site")
         .icon(BitmapDescriptorFactory
         .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
         
         map.addMarker(new MarkerOptions() 
         .position(juno_beach)
         .title("Juno Beach") .snippet("Juno Beach Debris Site")
         .icon(BitmapDescriptorFactory
         .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
		
	}




	private void help() {
		/** From the action bar, help opens URL for How_to_Help.java */
		Intent i = new Intent(this, How_to_Help.class);
		startActivity(i);
 	}
	
 	private void report() {
 		/** From the action bar, report opens URL for www.surveygizmo.com */
		Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
		surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
        startActivity(surveygizmo);
		
	}
 	private void w_help() {
 		/** From the action bar, w_help opens URL for MapActivity.java */
 		Intent i = new Intent(this, MapActivity.class);
 		startActivity(i);
 		
 	}

 	private void Harmful_Effects() {
 		/** From the action bar, Harmful_Effects opens URL for HarmfulEffects.java */
 		Intent i = new Intent(this, HarmfulEffects.class);
 		startActivity(i);
 		
 	}

 	private void Start_Game() {
 		/** From the action bar, Start_Game opens URL for SpotOn.java */
 		Intent i = new Intent(this, SpotOn.class);
 		startActivity(i);
 		
 	}

}