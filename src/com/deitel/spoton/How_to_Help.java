package com.deitel.spoton;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class How_to_Help extends Activity {
	/**Declares variables clean_ups and report*/
	Button clean_ups, report;
	@Override
	/**Oncreate links activity_how_to__help.xml to this Activity */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_how_to__help);
		clean_ups = (Button) findViewById(R.id.bclean_ups);
		report = (Button) findViewById(R.id.breport);
		
		clean_ups.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
			/** If clean-ups is selected, browser opens to http://www.dep.state.fl.us/coastal/programs/coral/debris1.htm*/
			Intent clean_ups = new Intent(android.content.Intent.ACTION_VIEW);
			clean_ups.setData(Uri.parse("http://www.dep.state.fl.us/coastal/programs/coral/debris1.htm"));
            startActivity(clean_ups);

			}
		});

		report.setOnClickListener(new View.OnClickListener() {

			@Override
			/** If report is selected, browser loads with http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated */
				public void onClick(View v) {
					// opening surveygizmo URL
				Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
				surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
                startActivity(surveygizmo);
			}
		});
	}

	/** Action bar is implemented*/
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			MenuInflater inflater = getMenuInflater();
			//getActionBar().setDisplayHomeAsUpEnabled(true);
			inflater.inflate(R.menu.main_action_bar, menu);
		    return super.onCreateOptionsMenu(menu);
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle presses on the action bar items
			/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
		    switch (item.getItemId()) {
		        case R.id.start_game:
		            Start_Game();
		            return true;
		        case R.id.harmful_effects:
		            Harmful_Effects();
		            return true;
		        case R.id.w_help:
		        	w_help();
		            return true;
		        case R.id.help:
		        	help();
		            return true;
		        case R.id.report:
		        	report();
		            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
		private void report() {
			/** From the action bar, report opens URL for www.surveygizmo.com */
			Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
			surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
	        startActivity(surveygizmo);
			
		}
		private void help() {
			/** From the action bar, help opens URL for How_to_Help.java */
			Intent i = new Intent(this, How_to_Help.class);
			startActivity(i);
		}

		private void w_help() {
			/** From the action bar, w_help opens URL for MapActivity.java */
			Intent i = new Intent(this, MapActivity.class);
			startActivity(i);
			
		}

		private void Harmful_Effects() {
			/** From the action bar, Harmful_Effects opens URL for HarmfulEffects.java */
			Intent i = new Intent(this, HarmfulEffects.class);
			startActivity(i);
			
		}

		private void Start_Game() {
			/** From the action bar, Start_Game opens URL for SpotOn.java */
			Intent i = new Intent(this, SpotOn.class);
			startActivity(i);
			
		}

}
