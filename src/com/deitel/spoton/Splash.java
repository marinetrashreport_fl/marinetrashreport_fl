package com.deitel.spoton;

import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.widget.VideoView;
import android.app.Activity;
import android.view.Menu;


public class Splash extends Activity {

    private final int SPLASH_DISPLAY_LENGHT = 2000;

    /** Splash page loads when the SpoitOn is called. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.logo); /** Set the logo.xml layout file*/
        MediaPlayer mp = MediaPlayer.create(getBaseContext(), R.raw.seagull); /**Gets soundfile from res/raw/seagull.mp3 */
        mp.start(); /** Opens mp.Start to start the sound */

        /** New Handler to start the Menu-Activity 
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(Splash.this,SpotOn.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
    }

    /**Code to add a video is included but not implemented */
/*public class Splash extends Activity implements OnCompletionListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
        VideoView video = (VideoView) findViewById(R.id.videoView);
        video.setVideoPath("android.resource://" + getPackageName() + "/"+ R.raw.splashtrashisland);
        video.start();
        video.setOnCompletionListener(this);
	}
	
	@Override
    public void onCompletion(MediaPlayer mp)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}*/

}
