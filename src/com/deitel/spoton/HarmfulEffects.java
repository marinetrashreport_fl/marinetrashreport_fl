package com.deitel.spoton;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class HarmfulEffects extends Activity {
	/** Creates four variables for the button selection, Plastic, Packing_material, Fishing_Gear*/
	Button Plastic, Packing_material, Fishing_Gear;

	@Override
	/** Oncreate links the harmfuleffects_horizontal_button.xml to the activity */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.harmfuleffects_horizontal_button);
		// liking xml buttons to java
		Plastic = (Button) findViewById(R.id.bPlastic);
		Fishing_Gear = (Button) findViewById(R.id.bFishing);
		Packing_material = (Button) findViewById(R.id.bPacking_material);
		

		// setting the listener
		Plastic.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				/** If Plastic button is selected, Plastic.java is opened*/
				Intent i = new Intent(HarmfulEffects.this, Plastic.class);
				startActivity(i);

			}
		});

		Fishing_Gear.setOnClickListener(new View.OnClickListener() {

			@Override
			/** If Fishing Gear button is selected, Fishing_Gear.java is opened*/
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(HarmfulEffects.this, Fishing_gear.class);
				startActivity(i);

			}
		});

		Packing_material.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				/** If Packing material button is selected, Packing_material.java is opened*/
				Intent i = new Intent(HarmfulEffects.this, Packing_material.class);
				startActivity(i);

			}
		});
	}

	/** Action bar is implemented*/
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			MenuInflater inflater = getMenuInflater();
			getActionBar().setDisplayHomeAsUpEnabled(true);
			inflater.inflate(R.menu.main_action_bar, menu);
		    return super.onCreateOptionsMenu(menu);
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
		    switch (item.getItemId()) {
		        case R.id.start_game:
		            Start_Game();
		            return true;
		        case R.id.harmful_effects:
		            Harmful_Effects();
		            return true;
		        case R.id.w_help:
		        	w_help();
		            return true;
		        case R.id.help:
		        	help();
		            return true;
		        case R.id.report:
		        	report();
		            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
		private void report() {
			/** From the action bar, report opens URL for www.surveygizmo.com */
			Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
			surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
	        startActivity(surveygizmo);
			
		}
		private void help() {
			/** From the action bar, help opens URL for How_to_Help.java */
			Intent i = new Intent(this, How_to_Help.class);
			startActivity(i);
			
		}

		private void w_help() {
			/** From the action bar, w_help opens URL for MapActivity.java */
			Intent i = new Intent(HarmfulEffects.this, MapActivity.class);
			startActivity(i);
			
		}

		private void Harmful_Effects() {
			/** From the action bar, Harmful_Effects opens URL for HarmfulEffects.java */
			Intent i = new Intent(HarmfulEffects.this, HarmfulEffects.class);
			startActivity(i);
			
		}

		private void Start_Game() {
			/** From the action bar, Start_Game opens URL for SpotOn.java */
			Intent i = new Intent(HarmfulEffects.this, SpotOn.class);
			startActivity(i);
			
		}

	}

