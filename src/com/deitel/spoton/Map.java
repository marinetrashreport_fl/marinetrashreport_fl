package com.deitel.spoton;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import com.google.android.gms.maps.*;

public class Map extends Activity {
	/** Create a simple google map, not used on the program*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);   
    }

    
 
    
    
    /** Action bar is implemented*/
 	@Override
 	public boolean onCreateOptionsMenu(Menu menu) {
 		// Inflate the menu; this adds items to the action bar if it is present.
 		MenuInflater inflater = getMenuInflater();
 		getActionBar().setDisplayHomeAsUpEnabled(true);
 		inflater.inflate(R.menu.main_action_bar, menu);
 	    return super.onCreateOptionsMenu(menu);
 	}
 	
 	@Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 	    // Handle presses on the action bar items
 		/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
 	    switch (item.getItemId()) {
 	        case R.id.start_game:
 	            Start_Game();
 	            return true;
 	        case R.id.harmful_effects:
 	            Harmful_Effects();
 	            return true;
 	        case R.id.w_help:
 	        	w_help();
 	            return true;
 	        case R.id.help:
 	        	help();
 	            return true;
 	        case R.id.report:
	        	report();
	            return true;
 	        default:
 	            return super.onOptionsItemSelected(item);
 	    }
 	}

 	private void help() {
 		/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
		Intent i = new Intent(this, How_to_Help.class);
		startActivity(i);
 	}
	
 	private void report() {
 		/** From the action bar, report opens URL for www.surveygizmo.com */
		Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
		surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
        startActivity(surveygizmo);
		
	}
 	private void w_help() {
 		/** From the action bar, w_help opens URL for MapActivity.java */
 		Intent i = new Intent(Map.this, MapActivity.class);
 		startActivity(i);
 		
 	}

 	private void Harmful_Effects() {
 		/** From the action bar, Harmful_Effects opens URL for HarmfulEffects.java */
 		Intent i = new Intent(Map.this, HarmfulEffects.class);
 		startActivity(i);
 		
 	}

 	private void Start_Game() {
 		/** From the action bar, Start_Game opens URL for SpotOn.java */
 		Intent i = new Intent(Map.this, SpotOn.class);
 		startActivity(i);
 		
 	}

}