// SpotOn.java
// Activity for the SpotOn app
package com.deitel.spoton;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RelativeLayout;

public class SpotOn extends Activity 
{

   private SpotOnView view; /** displays and manages the game */
   
   /** Called once SpoOn is called*/
   MediaPlayer mp; /** Create a MediaPlayer variable called mp*/
   @Override
   public void onCreate(Bundle savedInstanceState) 
   {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main); /** Set the main.xml to this activity*/
      mp = MediaPlayer.create(getBaseContext(), R.raw.wildplains); /**Gets soundfile from res/raw/wildplains.mp3 */
      mp.start(); /**Starts sound */
      /** create a new SpotOnView and add it to the RelativeLayout */
      RelativeLayout layout = 
         (RelativeLayout) findViewById(R.id.relativeLayout);
      view = new SpotOnView(this, getPreferences(Context.MODE_PRIVATE), 
         layout); 
      layout.addView(view, 0); /** add view to the layout */
   } // end method onCreate
   
   /** called when this Activity moves to the background */
   
@Override
   public void onPause()
   {
		/** Called when game is paused*/
	   super.onPause();
	   mp.stop();
	   view.pause(); /** view.pause releases resources held by the View */
	   } // end method onPause
	   
   // called when this Activity is brought to the foreground
   @Override
   public void onResume()
   {
	   /** Called onResume to resume the game*/
	   super.onResume();
	   view.resume(this); /** re-initialize resources released in onPause */
   } // end method onResume
   /** Action bar is implemented*/
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			MenuInflater inflater = getMenuInflater();
			getActionBar().setDisplayHomeAsUpEnabled(true);
			inflater.inflate(R.menu.main_action_bar, menu);
		    return super.onCreateOptionsMenu(menu);
		}
		
		@Override
		/** Action bar contains five options, start_game, harmful_effects, w_help, help and report*/
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle presses on the action bar items
		    switch (item.getItemId()) {
		        case R.id.start_game:
		            Start_Game();
		            return true;
		        case R.id.harmful_effects:
		            Harmful_Effects();
		            return true;
		        case R.id.w_help:
		        	w_help();
		            return true;
		        case R.id.help:
		        	help();
		            return true;
		        case R.id.report:
		        	report();
		            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}

		private void help() {
			/** From the action bar, help opens URL for How_to_Help.java */
			Intent i = new Intent(this, How_to_Help.class);
			startActivity(i);
		}
		
		private void report() {
			/** From the action bar, report opens URL for www.surveygizmo.com */
			Intent surveygizmo = new Intent(android.content.Intent.ACTION_VIEW);
			surveygizmo.setData(Uri.parse("http://www.surveygizmo.com/s3/943503/SEAFAN-Report-Form-BR-BR-Updated"));
	        startActivity(surveygizmo);
			
		}

		private void w_help() {
			/** From the action bar, w_help opens URL for MapActivity.java */
			Intent i = new Intent(SpotOn.this, MapActivity.class);
			startActivity(i);
			
		}

		private void Harmful_Effects() {
			/** From the action bar, Harmful_Effects opens URL for HarmfulEffects.java */
			Intent i = new Intent(SpotOn.this, HarmfulEffects.class);
			startActivity(i);
			
		}

		private void Start_Game() {
			/** From the action bar, Start_Game opens URL for SpotOn.java */
			Intent i = new Intent(SpotOn.this, SpotOn.class);
			startActivity(i);
			
		}

} /** end class SpotOn */



/**************************************************************************
 * (C) Copyright 1992-2012 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
